package ru.t1.avfilippov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public ProjectRemoveByIdRequest(@Nullable final String token) {
        super(token);
    }

}
