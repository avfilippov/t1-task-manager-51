package ru.t1.avfilippov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByProjectIdResponse extends AbstractTaskResponse {

    private List<TaskDTO> tasks;

    public TaskShowByProjectIdResponse(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
