package ru.t1.avfilippov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.endpoint.IAdminEndpoint;
import ru.t1.avfilippov.tm.api.service.IServiceLocator;
import ru.t1.avfilippov.tm.dto.request.SchemeDropRequest;
import ru.t1.avfilippov.tm.dto.request.SchemeInitRequest;
import ru.t1.avfilippov.tm.dto.response.SchemeDropResponse;
import ru.t1.avfilippov.tm.dto.response.SchemeInitResponse;
import ru.t1.avfilippov.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.IAdminEndpoint")
public final class AdminEndpoint implements IAdminEndpoint {

    @NotNull
    private final IServiceLocator serviceLocator;

    public AdminEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    @WebMethod
    public SchemeInitResponse initScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final SchemeInitRequest request
    ) {
        try {
            serviceLocator.getAdminService().initScheme(request.getToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeInitResponse();
    }

    @Override
    @NotNull
    public SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST) final @NotNull SchemeDropRequest request
    ) {
        try {
            serviceLocator.getAdminService().dropScheme(request.getToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeDropResponse();
    }

}
