package ru.t1.avfilippov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserProfileRequest extends AbstractUserRequest {

    public UserProfileRequest(@NotNull final String token) {
        super(token);
    }

}
