package ru.t1.avfilippov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.avfilippov.tm.api.repository.model.IProjectRepository;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;
import ru.t1.avfilippov.tm.model.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    protected Class<Project> getClazz() {
        return Project.class;
    }

    @Override
    @NotNull
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @Override
    @NotNull
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

}
